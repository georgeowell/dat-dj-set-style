const html = require('nanohtml')

const PlayerButton = require('./playerButton.js')
const infoBox = require('./infoBox')
const openingInfo = require('./openingInfo')

var player = new PlayerButton()

module.exports = (state, emit) => {
  if (state.set) {
    return html`
      <body id='meditation'>
      <div class='wrapper'>
         ${openingInfo(state, emit)}
         ${player.render(state, emit)}
         ${infoBox(state, emit)}
         <p id='question' onclick=${() => emit('toggle infoBox')}>
           ${infoToggle(state)}
         </p>
       </div>
    </body>
    `
  }
  function infoToggle (state) {
    if (state.infoBoxVisible) return html`X`
    return html`?`
  }
}
