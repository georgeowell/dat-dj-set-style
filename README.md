# Welcome to the 'DAT DJ SET STYLE' Zine Template

## Customizing This Template
You're likely finding this readme cos you checked out the source code of a cool song set shared with you, and you either wanna see how all this works, or you wanna copy this to make one yourself.

In either case...
**THESE ARE THE STEPS TO TAKE TO MAKE THIS ZINE YR OWN**:
  1.) If you haven't yet, copy this site by clicking the three dots next to share and selecting 'make a copy'
  2.) In your new copy, navigate to `distro/music` and replace the mp3 file there with your own.
  3.) Navigate, then, to `distro/info` and change this information with your own. 
  4.) Navigate to `AESTHETIC/colors-and-fonts.css` to change the major styling of the page.
  5.) Share with yr friends in the chorus, and share with me, ZACH! at webmaster@coolguy.website
  6.) Change this readme me to your own.  This whole thing is yours now, do what you want with it.

**There are readme's in each individual folder, with more details on what to do.**
`Note, if you're reading this on github, there is no music in the music folder cos I didn't want the repo to be gigantic. you can find the original music here: dat://baae72db38000ba1bb193a42235cbe62abbf2363767d5979d0de1963e2a37f08/`

At this point, the bulk is done, but if you really wanna personalize this through and through, you can:
1.) go to `AESTHETIC/main.css` and adjust all the styling for everything in the zine.
2.) go to `Javascripts` to adjust the structure, actions, and logic of the whole thing.  At that point, you've likely made something wholly new and cool so again please share with me cos I crave excitement and novelty.


The majority of changes can be done entirely within beaker using this here files page (though you can use your local copy and yr favorite text editor too).  If you want to adjust the javascript, it's made using [node](https://nodejs.org/en/) and [choo](https://choo.io).  Choo has a great tutorial on their page for getting started with this kinda development.

## Thank You's
  - Thanks to Beaker Browser and its team for this wonderful place.
  - Thanks to Kawaiipunk and [SSB](https://ssb.nz), for sharing the [original set](https://soundcloud.com/laipower/live-set-fast-forward-16-8-18) that inspired this here thing.
  - Thanks to [Raphael Bastide](https://raphaelbastide.com/) for the delightful code with the ssb hash worms.
  - Thanks to choo for the cute and accessible framework for building this.
  - Thanks to [smarkt](https://github.com/jondashkyle/smarkt), by jon-kyle, for giving a great way on building out the text files.
  - Thanks to the Scuttlebutt commuity for making cool shit all the time.
  - Thanks to you for reading this, and making all of this matter!

## Soundtrack
During creation I listened to:
  - a lot of Whitney Ballen's album, [You're a Shooting Star, I'm A Sinking Ship](https://whitneyballen.bandcamp.com/)
  - the lai power set linked above.
  - Four Tet's [New Energy](https://fourtet.bandcamp.com/album/new-energy)
  - American Pleasure Club's [A  Whole Fucking Lifetime of This](https://americanpleasureclub.bandcamp.com/album/a-whole-fucking-lifetime-of-this)

## Other Things

Spring starting up here in New Zealand, moving back with some friends on their beautiful airship home up in Hataitai, an empty kitchen table and a beautiful view of laundry whipping in the wind on the porch outside and the beautiful ocean just beyond. Taking breaks to eat Angelica's oat bars and go through a kundalini kriya with her.
